import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni'
export default defineConfig({
	plugins: [uni()],
	server: {
		port: 8080,
		proxy: {
			'/api': {
				// target: 'http://www.jjj-shop-enterprise.com',
				target: 'http://127.0.0.1:8000',//线上
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/api/, ''),
			},
		},
	},

});

